Run inteactive Conway's game of life on an Ableton Push 2.
You may need to edit your hardware address in the amidi command.

Top arrows are used to move view by 1, bottom arrows by 5.
Play button ticks one generation, record button erase the whole grid.


Using conway-php under MIT license:
https://github.com/igorw/conway-php